import React from 'react'
import propTypes from 'prop-types'

import { withStyles } from '@material-ui/core/styles'
import CloseIcon from '@material-ui/icons/Close'
import { Dialog, Slide, DialogTitle, IconButton } from '@material-ui/core'

const styles = {
    container: {
        display: 'flex',
        alignItems: 'flex-start',
    },
    paper: {
        padding: '2em',
        minWidth: '536px',
        '@media(max-width: 695.95px)': {
            minWidth: '80%',
            margin: 'auto 10%',
        },
    },
    title: {
        padding: 0,
        marginBottom: '1.5em',
    },
    close: {
        position: 'absolute',
        right: '1em',
        top: '1em',
        padding: 0,
        width: '1.4em',
        height: '1.4em',
        border: 'none',
        outline: 'none',
        backgroundColor: 'transparent',
        cursor: 'pointer',
        opacity: 1,
        transition: '.1s',
        color: 'inherit',
        '&:hover': {
            opacity: 0.7,
            backgroundColor: 'transparent',
        },
    },
    paperWidthSm: {
        '&$paperScrollBody': {
            '@media(max-width: 695.95px)': {
                margin: 'auto 10%',
            },
        },
    },
    paperScrollBody: {
        margin: 'auto',
    },
}

const Transition = props => <Slide direction="up" {...props} />

const Modal = ({
                   id,
                   isOpen,
                   onClose,
                   title,
                   children,
                   fullWidth,
                   classes,
                   size = 'sm',
                   scroll = 'body',
                   ...rest
               }) => (
    <Dialog
        id={id}
        open={isOpen}
        onClose={onClose}
        TransitionComponent={Transition}
        maxWidth={size}
        fullWidth={fullWidth}
        classes={{
            container: classes.container,
            paper: classes.paper,
            paperWidthSm: classes.paperWidthSm,
            paperScrollBody: classes.paperScrollBody,
        }}
        scroll={scroll}
        {...rest}
    >
        <DialogTitle onClose={onClose} className={classes.title}>
            {title && <div>{title}</div>}
            {onClose ? (
                <IconButton
                    id="close-modal-button"
                    className={classes.close}
                    onClick={onClose}
                >
                    <CloseIcon />
                </IconButton>
            ) : null}
        </DialogTitle>
        {children}
    </Dialog>
)

Modal.propTypes = {
    isOpen: propTypes.bool.isRequired,
    onClose: propTypes.func.isRequired,
    children: propTypes.node.isRequired,

    fullWidth: propTypes.bool,
    title: propTypes.string,
    size: propTypes.string,
    scroll: propTypes.string,
}

export default withStyles(styles)(Modal)
