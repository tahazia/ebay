import React, { Component } from 'react'
import propTypes from 'prop-types'

import {
    withStyles,
    createMuiTheme,
    MuiThemeProvider,
} from '@material-ui/core/styles/index'
import { DialogActions, DialogContent, TextField } from '@material-ui/core'
import Modal from './Modal'
import Button from './Button'

const theme = createMuiTheme({
    palette: {
        primary: {
            main: '#64de2e',
            dark: '#1fab0f',
        },
    },
    typography: {
        useNextVariants: true,
    },
})

const styles = {
    root: {
        padding: 0,
        marginBottom: '1.5em',
        overflow: 'visible',
    },
    actions: {
        margin: 0,
    },
    actionsButton: {
        margin: 0,
    },
    subtitle: {
        color: '#323237',
        marginBottom: '1em',
    },
    typography: {
        useNextVariants: true,
    },
}

class ContactModal extends Component {
    state = {
        email: '',
        message: '',
        errorName: '',
    };

    onChangeEmail = e => {
        this.setState({ errorName: false });
        this.setState({ email: e.target.value })
    };

    onChangeMessage = e => {
        this.setState({ message: e.target.value })
    };

    onClose = () => {
        this.setState({ email: '', message: '' });
        this.props.setVisibility(false)
    };

    onSubmit = () => {
        const { onAdd, setVisibility } = this.props;
        if (this.state.email.length > 0) {
            onAdd({ email: this.state.email, message: this.state.message });

            this.setState({ email: '', message: '' });
            setVisibility(false)
        }else {
            this.setState({ errorName: true })
        }
    };

    render() {
        const { title, classes, isVisible } = this.props;
        const { email, message, errorName } = this.state;

        return (
            <Modal
                id="add-modal"
                isOpen={isVisible}
                onClose={this.onClose}
                title={title}
            >
                <MuiThemeProvider theme={theme}>
                    <DialogContent className={classes.root}>
                        <TextField
                            fullWidth
                            id="add-modal-name-input"
                            label="Your email address"
                            value={email}
                            error={!!errorName}
                            onChange={this.onChangeEmail}
                            variant="outlined"
                            margin="normal"
                        />
                    </DialogContent>
                        <DialogContent className={classes.root}>
                            <TextField
                                fullWidth
                                id="add-modal-description-input"
                                label="Message"
                                variant="outlined"
                                multiline
                                rows="4"
                                value={message}
                                onChange={this.onChangeMessage}
                            />
                        </DialogContent>
                    <DialogActions className={classes.actions}>
                        <Button
                            id="add-modal-button"
                            type="submit"
                            title={'Add'}
                            onClick={this.onSubmit}
                        />
                    </DialogActions>
                </MuiThemeProvider>
            </Modal>
        )
    }
}

ContactModal.propTypes = {
    onAdd: propTypes.func.isRequired,
    title: propTypes.string.isRequired,
    isVisible: propTypes.bool.isRequired,
    setVisibility: propTypes.func.isRequired,
};

export default withStyles(styles)(ContactModal)
