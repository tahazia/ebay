import React, { Component } from "react";
import propTypes from "prop-types";

import { ButtonWrapper } from "./styles";

class Button extends Component {
    render() {
        const {
            id,
            type,
            align,
            width,
            title,
            height,
            onClick,
            selector,
            disabled
        } = this.props;
        return (
            <ButtonWrapper
                id={id}
                type={type}
                align={align}
                width={width}
                height={height}
                onClick={onClick}
                disabled={disabled}
                data-selector={selector}
            >
                {title}
            </ButtonWrapper>
        );
    }
}

Button.propTypes = {
    title: propTypes.string.isRequired,
    onClick: propTypes.func.isRequired
};

export default Button;
