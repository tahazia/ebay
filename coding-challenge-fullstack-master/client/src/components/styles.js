import styled, {keyframes} from 'styled-components'

// HEADER Styles

export const MainHeader = styled.header`
  display: flex;
  align-items: center;
  justify-content: center;
  min-height: 4.5em;
  background: #fff;
  width: 100%;
  position: relative;

  background: linear-gradient(90deg, rgba(53,72,180,1) 15%, rgba(201,37,37,1) 100%);

  -webkit-box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.05);
  box-shadow: 0 5px 15px 0 rgba(0, 0, 0, 0.05);
`;

export const HeaderLogo = styled.div`
  font-family: Arial;
  font-size: 30px;
  font-weight: 700;
  color: white;
  text-transform: none;
`;

// END

//BUTTON Styles

export const ButtonWrapper = styled.button`
  display: flex;
  align-items: center;
  align-self: center;
  justify-content: center;
  width: ${props => (props.width ? props.width : "-webkit-fill-available")};
  height: ${props => (props.height ? props.height : "3em")};
  border-radius: 2em;
  background: linear-gradient(90deg, rgba(100,222,46,1) 33%, rgba(31,171,15,1) 100%);

  &:hover {
    background: linear-gradient(90deg, rgba(100,222,46,0.8) 33%, rgba(31,171,15,0.8) 100%);
  }
  margin-left: auto;

  box-shadow: 2px 5px 10px -6px #f76b1c;
  cursor: pointer;
  outline-width: 0;

  border: none;
  border-radius: 2em;
  outline-width: 0;

  font-size: 0.875rem;
  font-weight: 500;
  text-align: center;
  color: #fff;
`;

//END BUTTON

//CARD Styles

const slideInFromLeft = keyframes`
  0% {
    transform: translateY(100%);
  }
  100% {
    transform: translateY(0);
  }
`;

export const CardContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;

  padding: 0.5em;

  border: 1px solid #dadada;
  border-radius: 4px;

  height: 320px;
  min-height: 320px;

  overflow: hidden !important;
  text-overflow: ellipsis;

  background: #dadada;

  &:hover {
    background: #a89ac8;;
    transform: scale(1.1);

    z-index: 3;
  }

  animation: 1s ease-out 0s 1 ${slideInFromLeft};
  transition: transform 0.2s linear;
`;

export const CardImage = styled.img`
  justify-content: center;
  align-items: center;
  display: block;
  border-radius: 5px 5px 0 0;
  width: 100%;
  max-height: 9.4em;
  object-fit: cover;
`;

export const TopCard = styled.div``;

export const BottomCard = styled.div``;

export const Title = styled.div`
  font-size: 1.2em;
  font-weight: 600;

  text-align: center;

  min-height: 2em;
`;


export const Detail = styled.div`
  padding: 0.5em;
  
  color: #606060;
  font-size: 1em;
`;

//END CARD
