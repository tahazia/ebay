import React, { Component } from "react";

import { MainHeader, HeaderLogo } from "./styles";

class Header extends Component {
    render() {
        return (
            <MainHeader>
                <HeaderLogo>eBay - Ads Listing</HeaderLogo>
            </MainHeader>
        );
    }
}

export default Header;
