import React, { Component } from "react";
import { withRouter } from "react-router-dom";

import Button from "./Button";
import {
    CardContainer,
    Title,
    Detail,
    TopCard,
    BottomCard,
    CardImage
} from "./styles";
import propTypes from "prop-types";

class AdCard extends Component {
    openPage = () => {
        const {id} = this.props;
        this.props.selectAd(id);
        this.props.history.push('/details-page/'+id);
    };

    render() {
        const { title, imageUrl, description } = this.props;

        return (
            <CardContainer>
                <TopCard>
                    <Title>{title}</Title>
                    <CardImage src={imageUrl} alt={title} />
                        <Detail>
                            <b>Description: </b> {description}
                        </Detail>
                </TopCard>

                <BottomCard>
                        <Button
                            title={"Contact"}
                            width={"9em"}
                            height={"2.25em"}
                            onClick={this.openPage}
                        />
                </BottomCard>
            </CardContainer>
        );
    }
}

AdCard.propTypes = {
    id: propTypes.number.isRequired,
    title: propTypes.string.isRequired,
    imageUrl: propTypes.string.isRequired,
    description: propTypes.string.isRequired,
};

AdCard.defaultProps = {
    title: "",
    imageUrl: "",
    description: "",
};

export default withRouter(AdCard);
