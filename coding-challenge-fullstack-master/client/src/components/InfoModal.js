import React, { Component } from 'react'
import {
    DialogActions,
    DialogContent,
    DialogContentText,
} from '@material-ui/core'
import Button from './Button'
import Modal from './Modal'
import propTypes from 'prop-types'

class InfoModal extends Component {
    onClose = () => {
        this.props.onClose(false)
    }

    render() {
        const { isVisible, title, description } = this.props;
        return (
            <Modal isOpen={isVisible} onClose={this.onClose} title={title}>
                <DialogContent>
                    <DialogContentText>{description}</DialogContentText>
                </DialogContent>
                <DialogActions>
                    <Button title={'Understood'} onClick={this.onClose} />
                </DialogActions>
            </Modal>
        )
    }
}

InfoModal.propTypes = {
    isVisible: propTypes.bool.isRequired,
    onClose: propTypes.func.isRequired,
    title: propTypes.string.isRequired,
    description: propTypes.string.isRequired,
}


export default InfoModal
