import React, { Component } from "react";

import {MainContainer, AdInformation, Title, Image, Detail, ContactSeller, ButtonContainer} from './styles'
import Button from '../components/Button'
import ContactModal from '../components/ContactModal'
import InfoModal from '../components/InfoModal'

class DetailsPage extends Component {
    state={
        showModal: false,
        infoModal: false,
        infoDescription: '',
    };

    setContactModal = condition => {
        this.setState({showModal: condition})
    };

    setDescription = desc => {
        this.setState({infoDescription: desc})
    };

    setInfoModal = condition => {
        this.setState({infoModal: condition})
    };

    onSend = info => {
        const data = {
            id: this.props.ad.id,
            email: info.email,
            message: info.message,
        };

        fetch('http://localhost:8080/ad-form-data', {
            method: 'post',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        })
            .then(function(res) {
            console.log(res)
        })
            .catch(err => {
            this.setDescription('Info could not be sent');
            console.log(err)
        });

        this.setInfoModal(true)

    };

    render() {
        const {ad} = this.props;
        return (
            <MainContainer>
                {ad &&
                <AdInformation>
                    <Title>{ad.title}</Title>
                    <Image src={ad.imageUrl} alt={'someTitle'} />
                    <Detail>{ad.description}</Detail>
                </AdInformation>
                }

                <ContactSeller>
                    <Title>Write a message</Title>
                    <ButtonContainer>
                    <Button title={"Send message"}
                            width={"25em"}
                            height={"3.25em"}
                            onClick={() => this.setContactModal(true)} />
                    </ButtonContainer>
                </ContactSeller>
                <ContactModal
                    isVisible={this.state.showModal}
                    title={'Send message'}
                    onAdd={this.onSend}
                    setVisibility={this.setContactModal}
                />
                <InfoModal
                    title={'Alert'}
                    isVisible={this.state.infoModal}
                    description={this.state.infoDescription}
                    onClose={this.setInfoModal}
                />
            </MainContainer>
        );
    }
}

export default DetailsPage;
