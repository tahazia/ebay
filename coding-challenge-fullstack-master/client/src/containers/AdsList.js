import React, { Component } from "react";
import AdCard from '../components/AdCard'

import {MainContainer, AdCardsContainter} from './styles'

class AdsList extends Component {

    render() {
        const {adsList, selectAd} = this.props;

        return (
            <MainContainer>
                <AdCardsContainter>
                    {adsList && adsList.map(ad => {
                        return (
                            <AdCard
                                key={ad.id}
                                id={ad.id}
                                title={ad.title}
                                imageUrl={ad.imageUrl}
                                description={ad.description}
                                selectAd={selectAd}
                            />
                        )
                    })}
                </AdCardsContainter>

            </MainContainer>
        );
    }
}

export default AdsList;
