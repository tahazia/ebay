import styled from 'styled-components'

export const MainContainer = styled.div`
  display: flex;
  flex-direction: column;
  position: relative;

  align-items: center;
  align-self: center;

  width: 100%;
  height: 100%;
`;

export const AdCardsContainter = styled.div`
  display: grid;

  width: 80%;
  height: 100%;

  margin-top: 2em;

  justify-content: center;
  align-self: center;

  grid-template-columns: repeat(auto-fit, minmax(340px, 1fr));
  grid-gap: 1em;
`;

export const AdInformation = styled.div`
    display: flex;
    position: relative;
    flex-direction: column;
    align-items: center;
    
    width: 80%;
    height: 60%;
    
    border: 1px solid #dadada;
    border-radius: 4px;
    
    margin: 2em 0em;
    text-overflow: ellipsis;
    
    background-color: #a89ac8;
`;

export const Title = styled.div`
  padding: 0.5em;
  
  font-size: 1.2em;
  font-weight: 600;
  
  min-height: 3em;
`;

export const Image = styled.img`
  justify-content: center;
  align-items: center;
  display: block;
  border-radius: 5px 5px 0 0;
  width: 80%;
  max-height: 12.4em;
  object-fit: cover;
`;

export const Detail = styled.div`
  padding: 0.5em;

  color: #606060;
  font-size: 1em;
`;

export const ContactSeller = styled.div`
    display: flex;
    position: relative;
    flex-direction: column;
    align-items: center;
    
    width: 80%;
    height: 20%;
    
    border: 1px solid #dadada;
    border-radius: 4px;
    
    background-color: #a89ac8;
`;

export const ButtonContainer = styled.div`
    justify-content: center;
    align-items: center;
`;
