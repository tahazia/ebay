import React, { Component } from 'react';
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';
import './App.css';

import Header from './components/Header'
import AdsList from './containers/AdsList'
import DetailsPage from './containers/DetailsPage'

import axios from "axios";

class App extends Component {
  state={
    ads: [],
      selectedAd: null,
  };

  componentDidMount() {
    this.getAdsList();
  }

  selectAd = id => {
    const ad = this.state.ads.filter(ad => ad.id === id);
    this.setState({selectedAd: ad[0]});
  };

  getAdsList = () => {
    axios
        .get("http://localhost:8080/ad-list")
        .then(response => {
          this.setAds(response.data)
        })
        .catch(error => {
          console.log(error);
        });
  };

  setAds = adsList => {
    this.setState({ads: adsList});
  };

  render() {
    return (
        <Router>
          <div className={'App'}>
            <Header />
            <Switch>
                <Route
                    path="/ad-list"
                    render={props => <AdsList {...props} adsList={this.state.ads} selectAd={this.selectAd} />}
                />
              <Route path='/details-page'
                     render={props => <DetailsPage {...props} ad={this.state.selectedAd} />} />
            </Switch>
          </div>
        </Router>
    );
  }
}

export default App;
